---
- name: User under with the service will run
  block:
  - name: Create the user that will run the springboot app {{ item.name }}
    user:
      name: '{{ item.user }}'
      home: '{% if item.user_home is defined %}{{ item.user_home }}{% else %}{{ item.install_dir }}{% endif %}'
      createhome: '{{ item.createhome | default(false) }}'
      shell: '{{ item.user_shell | default(springboot_user_shell) }}'
      system: '{{ item.system_user | default(springboot_system_user) }}'
    loop: '{{ springboot_apps }}'
    when: item.user is defined

  - name: Create the install directory of the springboot app {{ item.name }}
    file: dest={{ item.install_dir }} state=directory owner={{ item.user }} group={{ item.user }} recurse=yes
    loop: '{{ springboot_apps }}'
    when: item.user is defined

  - name: Create the log directory of the springboot app {{ item.name }}
    file: dest={{ item.log_dir }} state=directory owner={{ item.user }} group={{ item.user }} recurse=yes
    loop: '{{ springboot_apps }}'
    when: item.user is defined

  tags: [ 'springboot', 'springboot_user' ]

- name: Service artifact
  block:
  - name: Download the {{ item.name }} artifact from {{ maven_baseurl }}
    maven_artifact: artifact_id={{ item.maven_id }} version={{ item.maven_version | default('latest') }} group_id={{ item.maven_group_id }} extension={{ item.maven_extension | default('war') }} repository_url={{ item.maven_repo_url }} dest={{ item.install_dir }}/{{ item.name }}.{{ item.maven_extension | default('war') }}  verify_checksum=always mode='0644'
    with_items: '{{ springboot_apps }}'
    when: item.maven_repo_url is defined
    notify: Restart the springboot webservice

  tags: [ 'springboot', 'springboot_artifact' ]

- name: Service application configuration
  block:
  - name: Download the {{ item.name }} configuration from {{ item.remote_conf_url }} to use it as a template
    get_url: url={{ item.remote_conf_url }} dest=/var/tmp/.{{ item.app_conf_file }}.j2 owner=root group=root mode=0400
    with_items: '{{ springboot_apps }}'
    when: item.app_conf_file is defined
    delegate_to: 'localhost'

  - name: Install the {{ item.name }} configuration starting from the template
    template: src=/var/tmp/.{{ item.app_conf_file }}.j2 dest={{ item.install_dir }}/{{ item.app_conf_file }} owner=root group={{ item.user }} mode=0440
    with_items: '{{ springboot_apps }}'
    when: item.app_conf_file is defined
    notify: Restart the springboot webservice

  - name: Remove the local template file
    file: dest=/var/tmp/.{{ item.app_conf_file }}.j2 state=absent
    with_items: '{{ springboot_apps }}'
    when: item.app_conf_file is defined
    delegate_to: 'localhost'

  tags: [ 'springboot', 'springboot_configuration' ]

- name: Service logback configuration
  block:
  - name: Download the {{ item.name }} configuration from {{ item.logback_url }} to use it as a template
    get_url: url={{ item.logback_url }} dest=/var/tmp/.{{ item.logback_file }}.j2 owner=root group=root mode=0400
    with_items: '{{ springboot_apps }}'
    when: item.logback_file is defined
    delegate_to: 'localhost'

  - name: Install the {{ item.name }} configuration starting from the template
    template: src=/var/tmp/.{{ item.logback_file }}.j2 dest={{ item.install_dir }}/{{ item.logback_file }} owner=root group={{ item.user }} mode=0440
    with_items: '{{ springboot_apps }}'
    when: item.logback_file is defined
    notify: Restart the springboot webservice

  - name: Remove the local template file
    file: dest=/var/tmp/.{{ item.logback_file }}.j2 state=absent
    with_items: '{{ springboot_apps }}'
    when: item.logback_file is defined
    delegate_to: 'localhost'

  tags: [ 'springboot', 'springboot_configuration' ]

- name: Springboot startup files management
  block:
  - name: Install the springboot app upstart init file
    template: src=springboot-upstart.conf.j2 dest=/etc/init/{{ item.name }}.conf owner=root group=root mode=0644
    with_items: '{{ springboot_apps }}'
    when: ansible_service_mgr != 'systemd'

  - name: Install the springboot systemd service unit
    template: src=springboot.service.systemd.j2 dest=/etc/systemd/system/{{ item.name }}.service mode=0644 owner=root group=root
    with_items: '{{ springboot_apps }}'
    when: ansible_service_mgr == 'systemd'
    notify: systemd reload
    register: springboot_app_restart

  - name: Force all notified handlers to run at this point, not waiting for normal sync points
    meta: flush_handlers

  - name: Restart the springboot apps if the systemd unit changed
    service: name={{ item.name }} state=restarted
    with_items: '{{ springboot_apps }}'
    when: springboot_app_restart is changed

  - name: Install the springboot logrotate configuration
    template: src=springboot-logrotate.j2 dest=/etc/logrotate.d/{{ item.name }} owner=root group=root mode=0444
    with_items: '{{ springboot_apps }}'

  - name: Ensure that the springboot service is running and enabled
    service: name={{ item.name }} state=started enabled=yes
    with_items: '{{ springboot_apps }}'

  when: springboot_config
  tags: [ 'springboot', 'springboot_service' ]
