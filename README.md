Role Name
=========

A role that sets up a springboot service

Role Variables
--------------

The most important variables are listed below:

``` yaml
springboot_config: True
springboot_install_artifact: True
springboot_apps: 
  - { name: 'foo', log_dir: '/var/log/foo', install_dir: '/usr/lib/foo', state: 'present', app_conf_file: '', remote_conf_url: '', logback_url: '', logback_file: '', user: 'foo', java_opts: '', maven_repo_url: '', maven_id: '', maven_group_id: '', maven_extension: '', maven_version: '' }
```

Not all the elements are mandatory.

Dependencies
------------

None

License
-------

EUPL-1.2

Author Information
------------------

Andrea Dell'Amico, <andrea.dellamico@isti.cnr.it>
